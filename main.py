import _thread
import configparser
import logging
import time
from http.server import HTTPServer

import resquest
import common

logging.basicConfig(format='%(asctime)s - %(pathname)s[line:%(lineno)d] - %(levelname)s: %(message)s',
                    level=logging.DEBUG)

cf = configparser.ConfigParser()
cf.read("./config.ini", encoding="utf-8-sig")
host = ('0.0.0.0', 8888)


def startTask(threadName, ip, account, delay):
    while 1 == 1:
        logging.debug(ip)
        oid = cf.get("ups", "oid").split("\n")
        for s in oid:
            id = s.split(',')[0]
            title = s.split(',')[1]
            name = s.split(',')[2]
            type = s.split(',')[3]
            # 获取
            val = common.getOid(ip, account, id, type)
            resquest.dictList[name] = val

        time.sleep(delay)


# 启动http服务
def startHttp():
    # 启动一个http服务
    server = HTTPServer(host, resquest.Resquest)
    logging.debug("Starting server, listen at: %s:%s" % host)
    server.serve_forever()


if __name__ == '__main__':
    # 读取配置文件
    try:
        ip = cf.get("ups", "ip")
        account = cf.get("ups", "account")
        logging.debug("ip:" + ip + ",account:" + account)
        # 启动任务
        _thread.start_new_thread(startTask, ("startTask", ip, account, 2))
    except Exception:
        pass

    # 启动http
    startHttp()
