# http请求处理
import configparser
import json
from http.server import BaseHTTPRequestHandler

dictList = {}
cf = configparser.ConfigParser()
cf.read("./config.ini", encoding="utf-8-sig")


class Resquest(BaseHTTPRequestHandler):
    def do_GET(self):
        data = []
        fields = []
        oid = cf.get("ups", "oid").split("\n")
        item = {}
        for s in oid:
            id = s.split(',')[0]
            title = s.split(',')[1]
            name = s.split(',')[2]
            type = s.split(',')[3]
            item[name] = dictList[name]
            fields.append({
                'fieldName': name,
                'fieldType': 'text'
            })

        data.append(item)
        postData = {
            'code': 0,
            'message': '',
            'result': {
                'data': data,
                'fields': fields
            }
        }
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.end_headers()
        self.wfile.write(json.dumps(postData, ensure_ascii=False).encode())
